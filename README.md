# BCI using Motor Imagery Pattern Recognition
- This is my Course Project for AI & Computational Course
- **Aim:** Pattern recognition for a BCI experiment.
- Feature extraction from time signals of a Motor Imagery EEG data and feature selection using k-fold cross-validation.
- Classification using both MLP and RBF neural networks and genetic algorithms in MATLAB.
